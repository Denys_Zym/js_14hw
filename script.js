// Завдання
// Реалізувати можливість зміни колірної теми користувача.Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку "Змінити тему".
// При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки

const themeSwithers = document.querySelectorAll(".change-theme");

themeSwithers.forEach((i) => {
  i.addEventListener("click", function (e) {
    applyTheme(this.dataset.theme);
    localStorage.setItem("theme", this.dataset.theme);
  });
});

function applyTheme(themeName) {
  let themeUrl = `css/theme-${themeName}.css`;
  document.querySelector('[title = "theme"]').setAttribute("href", themeUrl);
}

let activeTheme = localStorage.getItem("theme");
if (!activeTheme) {
  applyTheme("1");
} else {
  applyTheme(activeTheme);
}

//-------------------------------------------------------------------------------

// const btn = document.querySelector("#theme");
// const themeStyle = document.querySelectorAll(`[theme = "themeStyle"]`);
// console.log(themeStyle);

// function chosenTheme() {
//   themeStyle.forEach((i) => {
//     if (localStorage.getItem("theme") === "dark") {
//       i.classList.add("dark");
//     } else {
//       i.classList.add("light");
//     }
//   });
// }
// function switchTheme() {
//   themeStyle.forEach((i) => {
//     if (i.classList.contains("light")) {
//       i.classList.remove("light");
//       i.classList.add("dark");
//       localStorage.removeItem("theme", "light");
//       localStorage.setItem("theme", "dark");
//     } else {
//       i.classList.remove("dark");
//       i.classList.add("light");
//       localStorage.removeItem("theme", "dark");
//       localStorage.setItem("theme", "light");
//     }
//   });
// }

// btn.addEventListener("click", switchTheme);

// chosenTheme();
